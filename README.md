# Require Revision Log Message

This allows you to require the log message to be viewed prior to saving or previewing a node revision.

## Usage

1. Node bundles this applies to can be configured at ```/admin/config/requireRevisionLog/adminsettings```

2. Set configuration and save changes

## Maintainers
* Kevin Finkenbinder (kwfinken) - https://www.drupal.org/u/kwfinken
* bluegeek9 (bluegeek9) -  https://www.drupal.org/u/bluegeek9
